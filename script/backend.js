
let Backend = {
    backendRequest: function(requestString, callback = undefined, blocking = false, method = 'get', postData = '') {
        console.log(':backend request');

        var req = new XMLHttpRequest();

        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                console.log(req.response);
                Backend.responses.push(JSON.parse(req.response));
                if (callback) { callback(); }
            }
            if (req.readyState == 4 && req.status != 200) {
                console.log(":backend ERROR(0)", req.status, req.response);
                Backend.responses.push('error');
                DomElements.updateEnginePanel();
            }
        }

        try {
            req.open(method, Backend.serverAddress() + '/' + requestString, !blocking);
            req.send(postData);
        } catch(error) {
            console.log(":backend ERROR(1)", error);
            Backend.responses.push('error');
            DomElements.updateEnginePanel();
        }
    },

    setModel: function() {
        console.log(':set model');
        var model = encodeURIComponent(Model.ImportExport.toString());
        if (model == null || model == '') {
            DomElements.setEngineError('Model has no regulations.');
            return;
        }
        Backend.backendRequest('set_model/' + model, function () {
            Backend.getInfo();
            DomElements.modelHasChangedShow(false);
        });
    },

    compute() {
        Backend.setModel();
        Backend.getResult();
    },

    getInfo: function() {
        console.log(':get info');
        Backend.backendRequest('get_info', function() {
            console.log(Backend.lastResponse());
            if (!Backend.lastResponse().status) {
                alert('Engine returned an error: ' + Backend.getError());
                DomElements.setEngineError(Backend.getError());
                return;
            }
            Backend.latestInfo = Backend.lastResponse().result;
            Backend.latestInfo.connected = true;
            DomElements.updateEnginePanel();
        }, true);
        Backend.triedToConnect = true;
    },

    getError() {
        if (Backend.lastResponse() == 'error')  {
            return 'Connection error.';
        }

        if (!Backend.lastResponse().status) {
            return Backend.lastResponse().message;
        }
        return null; 
    },

    getModel: function(modelCode = 'original') {
        console.log(':get model');
        Backend.backendRequest('get_model/' + modelCode, function() {
            if (!Backend.lastResponse().status) {
                alert('Engine returned an error: ' + Backend.getError()); 
                DomElements.setEngineError(Backend.getError());
                return;
            }
            Model.ImportExport.fromString(Backend.lastResponse().result);
            Visualization.fromModel();
            Visualization.layout('cose');
            DomElements.updateEnginePanel();
            DomElements.modelHasChangedShow(false);
        });
        DomElements.updateEnginePanel();
    },

    getResult: function() {
        console.log(':get result');
        Backend.backendRequest('get_result', function() {
            if (!Backend.lastResponse().status) {
                alert('Engine returned an error: ' + Backend.getError()); 
                DomElements.setEngineError(Backend.getError());
                return;
            }
            DomElements.generateResultTable(Backend.lastResponse().result);
            Backend.latestResult = Backend.lastResponse().result;
            Backend.getInfo();
            console.log('RESULT IS ', Backend.latestResult);
            DomElements.generateResultTable(Backend.latestResult);
            DomElements.showPanel('result')
            DomElements.updateEnginePanel();
        });
        DomElements.updateEnginePanel({ forceBusy: true });
    },

    sbmlToAeon: function(sbml) {
        console.log(':backend:import from sbml');
        Backend.backendRequest('sbml_to_aeon', function() {
            if (!Backend.lastResponse().status) {
                alert('Engine returned an error: ' + Backend.getError()); 
                DomElements.setEngineError(Backend.getError());
                return;
            }
            Model.ImportExport.fromString(Backend.lastResponse().result);
            Visualization.fromModel();
        },
        false,
        'post',
        'data=' + sbml
        );
    },

    lastResponse: function() {
        var l = Backend.responses.length;
        return Backend.responses[l - 1];
    },

    serverAddress: function() {
        return DomElements.addressInput.value;
    },

    latestInfo: { connected: false },
    latestResult: undefined,
    triedToConnect: false,
    responses: [],
};
