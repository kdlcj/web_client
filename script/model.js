
let Model = {
    /* Add a new edge to the model:
       from: name of a node
       to: name of a node
       visible: boolean value
       kind: one of the following: 'inhibition', 'activation', 'unspecified'
    */
    addEdge: function(from, to, visible, kind, changeEvent = true) {
        let succ = Model.successors;
        if (Model.existsEdge(from, to)) {
            return false;
        }
        
        Model.addNode(from, changeEvent);
        Model.addNode(to, changeEvent);

        var i = 0;
        while (succ[i].to != to) {
            i++;
        }

        if (succ[i].successorList.length == 0) {
            succ[i].successorList.push({ from: from, visible: visible, kind: kind });
            if (changeEvent) {
                Model.changeEvent();
            }
            return true;
        }

        var j = 0;
        while (j < succ[i].successorList.length && succ[i].successorList[j].from < from) {
            j++;
        }
        succ[i].successorList.splice(j, 0, { from: from, visible: visible, kind: kind });

        if (changeEvent) {
            Model.changeEvent();
        }
        return true;
    },

    /* Add a new node: id is a name string */
    addNode: function(id, changeEvent = true) {
        if (Model.existsNode(id)) {
            return false;
        }

        let succ = Model.successors;
        if (succ.length == 0) {
            succ.push({ to: id, successorList: [], constraints: '' });
            if (changeEvent) {
                Model.changeEvent();
            }
            return true;
        }

        var i = 0;
        while (i < succ.length && succ[i].to < id) {
            i++;
        }
        succ.splice(i, 0, { to: id, successorList: [], constraints: '' });

        if (changeEvent) {
            Model.changeEvent();
        }
        return true;
    },

    addNodeConstraint: function(nodeName, updateFunctionString) {
        for (var i = 0; i < Model.successors.length; i++) {
            if (nodeName == Model.successors[i].to) {
                Model.successors[i].constraints = updateFunctionString;
                break;
            }
        }

        Model.changeEvent();
    },

    changeEdgeObservability: function(from, to, observable) {
        for (var i = 0; i < Model.successors.length; i++) {
            if (to == Model.successors[i].to) {
                for (var j = 0; j < Model.successors[i].successorList.length; j++) {
                    if (Model.successors[i].successorList[j].from == from) {
                        Model.successors[i].successorList[j].visible = observable;
                        Model.changeEvent({ ignore: true });
                        return;
                    }
                }
            }
        }    
    },

    changeEdgeKind: function(from, to, newKind) {
        for (var i = 0; i < Model.successors.length; i++) {
            if (to == Model.successors[i].to) {
                for (var j = 0; j < Model.successors[i].successorList.length; j++) {
                    if (Model.successors[i].successorList[j].from == from) {
                        Model.successors[i].successorList[j].kind = newKind;
                        Model.changeEvent({ ignore: true });
                        return;
                    }
                }
            }
        }    
    },

    /* add function that will be called on model change */
    addOnChangeEvent: function(fn) {
        Model.changeEventFunctions.push(fn);
    },

    changeEvent: function(arg) {
        for (var i = 0; i < Model.changeEventFunctions.length; i++) {
            Model.changeEventFunctions[i](arg);
        }
    },

    existsNode: function(id) {
        let succ = Model.successors;
        for (var i = 0; i < succ.length; i++) {
            if (succ[i].to == id) {
                return true;
            }
        }
        return false;
    },

    renameNode: function(idOld, idNew) {
        console.log(':model:renameNode');

        if (Model.existsNode(idNew)) {
            return false;
        }

        let succ = Model.successors;
        for (var i = 0; i < succ.length; i++) {
            if (succ[i].to == idOld) {
                succ[i].to = idNew;
            }
            for (var j = 0; j < succ[i].successorList.length; j++) {
                if (succ[i].successorList[j].from == idOld) {
                    succ[i].successorList[j].from = idNew;
                }
            }
            succ[i].successorList.sort((a, b) => a.from > b.from? 1: -1);
        }

        Model.successors.sort((a, b) => a.to > b.to? 1: -1);
        Model.changeEvent();

        return true;
    },

    removeEdge: function(from, to) {
        console.log(':model:removeEdge');

        if (!Model.existsEdge(from, to)) {
            return false;
        }
        let succ = Model.successors;
        var i = 0;
        while (i < succ.length && succ[i].to != to) {
            i++;
        }
        
        var j = 0;
        while (j < succ[i].successorList.length && succ[i].successorList[j].from != from) {
            j++;
        }

        succ[i].successorList.splice(j, 1);

        Model.changeEvent();
        return true;
    },

    removeNode: function(id) {
        if (!Model.existsNode(id)) {
            return true;
        }

        var i = 0;
        let succ = Model.successors;
        while (i < succ.length && succ[i].to != id) {
            i++;
        }
        succ.splice(i, 1);

        succ.forEach((succList) => {
            var succList = succList.successorList;
            var index = succList.findIndex(x => x.from == id);
            if (index >= 0) {
                succList.splice(succList.findIndex(x => x.from == id));
            }
        });

        Model.changeEvent();
        return true;
    },

    numberOfNodes: function() {
        return Model.successors.length;
    },

    numberOfEdges: function() {
        return Model.successors.map(list => list.successorList.length).reduce((a, b) => a + b, 0);
    },

    maxIndegree: function() {
        return Model.successors.map(list => list.successorList.length).reduce((a, b) => Math.max(a, b), 0);
    },

    existsEdge: function(from, to) {
        let succ = Model.successors;
        for (var i = 0; i < succ.length; i++) {
            if (succ[i].to == to) {
                for (var j = 0; j < succ[i].successorList.length; j++) {
                    if (succ[i].successorList[j].from == from) {
                        return true;
                    }
                }
            }
        }
        return false;
    },

    getRegulation: function(from, to) {
        let succ = Model.successors;
        for (var i = 0; i < succ.length; i++) {
            if (succ[i].to == to) {
                for (var j = 0; j < succ[i].successorList.length; j++) {
                    if (succ[i].successorList[j].from == from) {
                        return succ[i].successorList[j];
                    }
                }
            }
        }

        return null;
    },

    consoleLogWrite: function() {
        Model.successors.forEach(el => {
            el.successorList.forEach(from => {
                console.log(from.from, '>>', el.to);
            });
        });
    },

    ImportExport: {
        toFile: function(filename = 'network.aeon') {
            var el = document.createElement('a');
            el.setAttribute('href', 'data:text/plain;charset=utf-8,'
                                    + encodeURIComponent(Model.ImportExport.toString()));
            el.setAttribute('download', filename);
            el.style.display = 'none';
            document.body.appendChild(el);
            el.click();
            document.body.removeChild(el);
        }, 

        fromString: function(string) {
            Model.successors = [];
            var constraints = {};
            const dict = {
                '-?':  ['unspecified', true],
                '-??': ['unspecified', false],
                '->':  ['activation',  true],
                '->?': ['activation',  false],
                '-|':  ['inhibition',  true],
                '-|?': ['inhibition',  false],
            };
            string.split(/\n/).forEach(line => {
                const els = line.trim().split(/\s+/);
                if (line == '') {
                    return;
                }

                if (els[0][0] == '$') {
                    const match = line.match(/\$.+:/);
                    if (match) {
                        const species = match[0].slice(1, -1).trim();
                        const updateFn = line.split(':')[1].trim();

                        constraints[species] = updateFn;
                    }
                    return;
                }
                Model.addEdge(els[0], els[2], dict[els[1]][1], dict[els[1]][0], false);
            });

            for (var i = 0; i < Model.successors.length; i++) {
                const c = constraints[Model.successors[i].to];
                if (c) {
                    Model.successors[i].constraints = c; 
                }
            }
            Model.changeEvent();
        },

        toString: function() {
            result = '';
            Model.successors.forEach(el => {
                if (el.constraints != '') {
                    result += '$ ' + el.to + ': ' + el.constraints + '\n';
                }
                el.successorList.forEach(from => {
                    result += from.from + ' '
                           + { 'activation': '->', 'inhibition': '-|', 'unspecified': '-?' }[from.kind]
                           + (from.visible? '': '?')
                           + ' ' + el.to + '\n';
                });
            });

            return result;
        },
    },

    successors: [],
    changeEventFunctions: [],
};
