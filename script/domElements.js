
let DomElements = {
    init: function() {
        console.log(':initialize elements');
        const get = function(name) { return document.getElementById(name) };
        const getClass = function(name) { return document.getElementsByClassName(name) };

        this.selectionNodeMenu  = get('clickNodeMenu');
        this.selectionEdgeMenu  = get('clickEdgeMenu');
        this.textInputBox       = get('textInput');
        this.textInputWindow    = get('textInputWindow');
        this.paramTable         = get('paramTable');
        this.modelMenuStatus    = get('modelMenuStatus');
        this.layoutMenuStatus   = get('layoutMenuStatus');
        this.cyContainer        = get('container');
        this.engineIndicator    = get('engineIndicator');
        this.engineIndicatorBig = get('engineIndicatorBig');
        this.resultTable        = get('resultTable');
        this.renameTextInputBox = get('renameTextInput');
        this.addressInput       = get('addressInput');

        this.importExportPanel  = get('importExportPanel');
        this.modelPanel         = get('modelPanel');
        this.resultPanel        = get('resultPanel');
        this.enginePanel        = get('enginePanel');
        this.layoutPanel        = get('layoutPanel');

        DomElements.engineButtons.connect       = get('connectToEngineButton');
        DomElements.engineButtons.uploadModel   = get('uploadModelButton');
        DomElements.engineButtons.downloadModel = get('downloadModelButton');
        DomElements.engineButtons.getResult     = get('getResultButton');
        DomElements.engineButtons.importSbmlP   = get('importSbmlP');
        DomElements.engineButtons.importSbmlI   = get('importSbmlI');
        DomElements.engineButtons.exportSbmlP   = get('exportSbmlP');
        DomElements.engineButtons.exportSbmlI   = get('exportSbmlI');
        DomElements.engineButtons.menuCompute   = get('menuComputeButton');

        DomElements.statusPanels.result       = get('resultPanelStatus');
        DomElements.statusPanels.importExport = get('importExportStatus');
        this.engineMenuStatus   = get('engineMenuStatus');

        this.modelListContainer = get('modelListContainer');
        this.nodeRenamingHidden = getClass('nodeRenamingButton');
        this.nodeRenamingShow   = getClass('nodeOption');
    },

    hideSelectionMenu: function() {
        console.log(':hide selection menus');
        this.selectionEdgeMenu.style.display = 'none';
        this.selectionNodeMenu.style.display = 'none';
        DomElements.showRenameMenu();
    },

    generateResultTable: function(json) {
        console.log(':generate result table', json);
        var result = json.sort((a, b) => b.sat_count - a.sat_count);
        if (!result) {
            return false;
        }

        const paramsNum = result.reduce((acc, curr) => acc + curr.sat_count, 0);
        DomElements.statusPanels.result.innerText = 'total parametrizations: ' + paramsNum;

        var table = '';
        result.forEach(({ sat_count, phenotype })=> {
            var behavior = phenotype.map(x => x[0]).sort().join('');
            table += '<tr><td class="tableSatCount">' + sat_count
                  +  '</td><td class="tableBehavior">'
                  +  behavior + '</td><td><span onclick="GuiClick.getWitness(\''
                  +  behavior + '\')" class="menuButton">witness<span></td></tr>';
        });

        table = "<table><tr id='tableHead'><td>CARDINALITY</td><td>BEHAVIOR CLASS</td></tr>" + table + "<table>";
        this.resultTable.innerHTML = table;
        return true;
    },

    updateLayoutInfo: function() {
        DomElements.layoutMenuStatus.innerText = Visualization.layoutName;
    },

    updateImportExportInfo: function() {
        if (Model.successors.length == 0) {
            DomElements.statusPanels.importExport.innerText = 'No model loaded.\nImport it, or use the editor to create one.';
        } else {
            DomElements.statusPanels.importExport.innerText = '';
        }


        document.getElementById('localStorageImport').className = 'menuButtonInactive';
        try {
            if (localStorage.getItem('model') != null) {
                document.getElementById('localStorageImport').className = 'menuButton';
            }
        } catch (e) {}
    },

    updateModelInfo: function() {
        console.log(':update model info');
        if (Model.successors.length == 0) {
            DomElements.modelMenuStatus.innerText = "No model loaded.\nImport it, or use the editor to create one.";
            document.getElementById('noModelMessage').style.display = 'block';
            return;
        }
        document.getElementById('noModelMessage').style.display = 'none';
        DomElements.modelMenuStatus.innerText = Model.numberOfNodes() + ' species, ' + Model.numberOfEdges()
                                              + ' regulations. Maximum indegree: ' + Model.maxIndegree();
    },

    updateEnginePanel: function(forceBusy = {}) {
        console.log(':update engine options');
        console.log('force busy is ', forceBusy);
        const state = {
            info: Backend.latestInfo,
            triedToConnectAlready: Backend.triedToConnect,
            connected: Backend.latestInfo.connected,
        };

        console.log(state);
        const toggler = (condition, name) => condition? DomElements.engineButtons[name].className = 'menuButton' 
                                                      : DomElements.engineButtons[name].className = 'menuButtonInactive';
        
        const sbml = (cond) => {
            toggler(cond, 'importSbmlI');
            toggler(cond, 'importSbmlP');
            toggler(cond, 'exportSbmlI');
            toggler(cond, 'exportSbmlP');
            const state = ['hidden', 'visible'];
            document.getElementById('sbmlIOengine').style.visibility = state[cond?0:1];
        };

        if (!state.connected) {
            sbml(false);
            toggler(false, 'uploadModel');
            toggler(false, 'downloadModel');
            toggler(false, 'getResult');
            DomElements.setEngineStatus('not connected');
            if (state.triedToConnectAlready) {
                DomElements.engineButtons.connect.innerText = 'retry to connect';
                DomElements.setIndicatorColor('red');
                DomElements.setEngineStatus('unable to connect');
            } else {
                DomElements.setIndicatorColor('grey');
            }

            return;
        }

        sbml(true);
        if (state.info.busy || (forceBusy && forceBusy.forceBusy)) {
            DomElements.setIndicatorColor('blue');
            toggler(false, 'menuCompute');
            DomElements.engineButtons.menuCompute.innerText = 'waiting for result';
            DomElements.setEngineStatus('engine is busy');
        } else {
            DomElements.engineButtons.menuCompute.innerText = 'compute';
            toggler(true, 'menuCompute');
            DomElements.setIndicatorColor('green');
            DomElements.setEngineStatus('engine is ready');
        }
        DomElements.engineButtons.connect.innerText = 'reconnect';

        if (Backend.getError() != undefined) {
            DomElements.setEngineStatus(Backend.getError());
            DomElements.setIndicatorColor('orange');
            return;
        }

        if (state.info.has_result) {
            DomElements.engineButtons.getResult.innerText = 'download result';
        } else {
            DomElements.engineButtons.getResult.innerText = 'compute';
        }

        toggler(state.info.has_model, 'getResult');
        if (Model.successors.length == 0) {
            toggler(false, 'uploadModel');
        } else {
            toggler(true, 'uploadModel');
        }

        toggler(state.info.has_model, 'downloadModel');
    },

    setEngineError(message) {
        if (message == 'Connection error.') {
            DomElements.setIndicatorColor('red');
            DomElements.setEngineStatus('unable to connect');
            return;
        } 
        DomElements.setIndicatorColor('orange');
        DomElements.setEngineStatus(message);
    },

    generateModelTable: function() {
        console.log(':generate model table');
        let div = (cont, opt = '') => '<div ' + opt + '>' + cont + '</div>';
        var result = '';
        let dict = { 'activation': 'activation', 'inhibition': 'inhibition', 'unspecified': 'regulation'};

        Model.successors.forEach(el => {
            result += '<div onmouseout="GuiClick.speciesCellHover(\'' + el.to + '\', false)"'
                   + ' onmouseover="GuiClick.speciesCellHover(\'' + el.to + '\')" id="speciesCell'
                   + el.to + '" class="modelTableSpecies">'
                   + '<input onchange="GuiClick.renameSpecies(\'' + el.to
                   + '\', this.value)" class="speciesNameInputBox" spellcheck="false" type="text" value="'
                   + el.to + '">'
                   //+ div('delete', 'class="menuButton"')
                   //+ div('add regulation', 'class="menuButton"')
                   + div('Regulators:', 'class="modelh3"');
            if (el.successorList.length == 0) {
                result += div('(none)', 'class="noRegulationsMessage"');
            }
            el.successorList.forEach(from => {
                result += '<div class="modelTableRegulation">'
                       + div(from.from, 'class="modelTableFrom"')
                       + div((from.visible? 'observable ': 'non-observable '), 'class="modelTableObservability"'
                       + ' onclick="GuiClick.toggleRegulationObservability(\''+from.from+'\', \''+el.to+'\', this)"')
                       + div((dict[from.kind]), 'class="modelTableArrow" onclick="GuiClick.toggleRegulationKind(\''+from.from+'\',\''+el.to+'\', this)"')
                       + '</div>';
            });
            result += div('Update function:', 'class="modelh3"')
                   +  '<input onchange="GuiClick.changeSpeciesUpdateFn(\'' + el.to
                   +'\', this.value)" placeholder="(default)" class="constraintTextBox" type="text" '
                   + (el.constraints != ''? 'value="' + el.constraints + '"': '') + 'spellcheck="false"></div>';
        });

        return result;
    },

    modelHasChangedMessage() {
        if (Backend.latestInfo && Backend.latestInfo.has_model) {
            DomElements.modelHasChangedShow(true);
        }
    },

    modelHasChangedShow(show = true) {
        document.getElementById('modelHasChanged').style.visibility = show? 'visible': 'hidden';
    },

    updateModelTable: function(ignore = {}) {
        if (ignore == null || !ignore.ignore) {
            DomElements.modelListContainer.innerHTML = DomElements.generateModelTable();
        }
    },

    updateModelPanel: function(ignoreTable = false) {
        DomElements.updateModelTable(ignoreTable);
        DomElements.updateModelInfo()
    },

    showPanel: function(name) {
        DomElements.enginePanel.style.display = 'none';
        DomElements.modelPanel.style.display = 'none';
        DomElements.layoutPanel.style.display = 'none';
        DomElements.importExportPanel.style.display = 'none';
        DomElements.resultPanel.style.display = 'none';

        switch (name) {
        case 'engine':       DomElements.enginePanel.style.display = 'block'; break;
        case 'model':        DomElements.modelPanel.style.display = 'block'; break;
        case 'layout':       DomElements.layoutPanel.style.display = 'block'; break;
        case 'importExport': DomElements.importExportPanel.style.display = 'block'; break;
        case 'result':       DomElements.resultPanel.style.display = 'block'; break;
        default:
        }
    },

    showRenameMenu: function(showMenu = true) {
        console.log(':show rename menu');
        var show = this.nodeRenamingShow;
        var hide = this.nodeRenamingHidden;
        if (!showMenu) {
            [show, hide] = [hide, show];
        }
        for (i = 0; i < show.length; i++) {
            show[i].style.display = 'block'; 
        }
        for (i = 0; i < hide.length; i++) {
            hide[i].style.display = 'none';
        }
    },

    setIndicatorColor: function(color) {
        var niceRed    = '#d05d5d';
        var niceGreen  = '#4abd73';
        var niceBlue   = '#3a568c';
        var niceGrey   = '#797979';
        var niceOrange = '#e6c620';

        var colorCode = niceGrey;

        switch (color) {
        case 'red': colorCode = niceRed; break;
        case 'blue': colorCode = niceBlue; break;
        case 'green': colorCode = niceGreen; break;
        case 'orange': colorCode = niceOrange; break;
        default: colorCode = niceGrey;
        }

        DomElements.engineIndicator.style.color = colorCode;
        DomElements.engineIndicatorBig.style.color = colorCode;
    },

    setEngineStatus: function(message) {
        console.log(':set engine status');
        engineMenuStatus.innerText = message;
    },

    constants: {
        niceRed: '#d05d5d',
        niceGreen: '#4abd73',
        niceBlue: '#3a568c',
        niceGrey: '#797979',
    },

    engineButtons: {},
    statusPanels: {},

    selectionEdgeMenu:  undefined,
    engineMenuStatus:   undefined,
    modelMenuStatus:    undefined,
    selectionNodeMenu:  undefined,
    textInputBox:       undefined,
    textInputWindow:    undefined,
    paramTable:         undefined,
    cyContainer:        undefined,
    engineIndicator:    undefined,
    engineIndicatorBig: undefined,
    resultTable:        undefined,
};
