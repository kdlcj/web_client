
let Visualization = {
    initialize: function(domElements = DomElements, model = Model, guiClick = GuiClick) {
        Visualization.cy = cytoscape(Visualization.cytoscapeOptions(domElements));
        Visualization.eh = Visualization.cy.edgehandles(Visualization.edgehandlesOptions);
        Visualization.cy.on('select', ev => guiClick.selectElement(ev));
        Visualization.cy.on('unselect', ev => guiClick.selectElement(ev, false));
        Visualization.cy.on('grab', () => { Visualization.layoutName = 'custom'; Visualization.layoutChangeEventFunctions.forEach(f => f()) });
        Visualization.onRename.push((oldName, newName) => model.renameNode(oldName, newName));
        Visualization.onRename.push(() => domElements.modelListContainer.innerHTML = domElements.generateModelTable());
    },

    layout: function(name) {
        if (name == 'center') {
            Visualization.cy.center();
            return;
        }

        if (name == 'fit') {
            Visualization.cy.fit();
            return;
        }

        let options = {
            cose: {
                name: 'cose',
                padding: 250,
                animate: true,
                nodeRepulsion: function(node) { return 100000; },
                animate: true,
                animationDuration: 300,
                refresh: 20,
                fit: true,
                nodeDimensionsIncludeLabels: true,
            },
            grid: {
                name: 'grid',
                condense: false,
                rows: Math.floor(Math.sqrt(Model.numberOfNodes())),
                animate: true,
                animationThreshold: 250,
                animationDuration: 300,
            },
            random: {
                name: 'random',
                animate: true,
                animationDuration: 300,
                animationThreshold: 250,
            },
            circle: {
                name: 'circle',
                animate: true,
                animationThreshold: 250,
                animationDuration: 300,
            },
        };

        
        if (options[name]) {
            Visualization.cy.layout(options[name]).start();
        }
        Visualization.layoutName = name;
        Visualization.layoutChangeEventFunctions.forEach(f => f());
    },

    align: function() {
        Visualization.cy.fit();
    },

    addNodeConstraint: function(name, add = true) {
        const node = Visualization.cy.nodes('[name="' + name + '"]');
        if (node) {
            node.data().hasUpdateFn = add? 'true': 'false';
            if (!add) {
                node.data().hasUpdateFn = 'false';
            }
            Visualization.update();
        }
    },

    addOnLayoutChangeEvent: function(fn) {
        Visualization.layoutChangeEventFunctions.push(fn);
    },

    addNode: function(name, position) {
        console.log(position);
        if (Visualization.cy.nodes('[name="' + name + '"]').length == 0) {
            Visualization.cy.add({ position, data: { name: name, hasUpdateFn: 'false' }})
                            .on('mouseover', Visualization.mouseOverNode)
                            .on('mouseout', (e) => Visualization.mouseOverNode(e, false));
        }
    },

    mouseOverNode: function(ev, show = true) {
        GuiClick.speciesCellHover(ev.target.data().name, show);
    },

    fromModel: function() {
        Visualization.cy.nodes().remove();
        var result = [];

        Model.successors.forEach(el => {
            Visualization.cy.add({ data: { id: el.to, name: el.to, hasUpdateFn: el.constraints == ''? 'false': 'true' }})
                            .on('mouseover', Visualization.mouseOverNode)
                            .on('mouseout', (e) => Visualization.mouseOverNode(e, false));
        });

        Model.successors.forEach(el => {
            el.successorList.forEach(from => {
                Visualization.cy.add({ data: {
                    source: from.from,
                    visible: from.visible? '': '?',
                    kind: from.kind,
                    target: el.to,
                }});
            });
        });
        Visualization.cy.fit();
    },

    renameNode: function(oldName, newName) {
        if (Visualization.cy.nodes('[name="' + newName + '"]').length != 0) {
            return false;
        }

        if (Visualization.cy.nodes('[name="' + oldName + '"]').length == 0) {
            return true;
        }

        Visualization.cy.nodes('[name="' + oldName + '"]').data().name = newName;
        Visualization.update();

        Visualization.onRename.forEach(f => f(oldName, newName));
        return true;
    },

    /* `observable` is one of the following: '', '?' */
    changeEdgeObservability: function(from, to, observable) {
        from = Visualization.nodeNameToId(from);
        to = Visualization.nodeNameToId(to);
        let edge = Visualization.cy.edges('[source="' + from + '"][target="' + to + '"]');
        if (edge) {
            edge.data().visible = observable;
            Visualization.update();
            return true;
        }

        return false;
    },

    nodeIdToName: function(id) {
        return Visualization.cy.elements('[id="' + id + '"]').data().name;
    },

    nameOfSelectedNode: function() {
        const nodes = Visualization.cy.nodes(':selected');

        if (nodes.length != 0) {
            return nodes[0].data().name;
        }
        return null;
    },

    sourceTargetOfSelectedEdge: function() {
        const edges = Visualization.cy.edges(':selected');
        if (edges.length != 0) {
            var e = edges[0].data();
            return {
                source: Visualization.nodeIdToName(e.source),
                target: Visualization.nodeIdToName(e.target)
            };
        }
        return null;
    },

    nodeNameToId: function(name) {
        return Visualization.cy.elements('[name="' + name + '"]').data().id;
    },

    /* `kind` is one of the following: 'activation', 'inhibition', 'unspecified' */
    changeEdgeKind: function(from, to, kind) {
        from = Visualization.nodeNameToId(from);
        to = Visualization.nodeNameToId(to);
        let edge = Visualization.cy.edges('[source="' + from + '"][target="' + to + '"]');
        if (edge) {
            console.log('edge IS', edge, 'input', from, to);
            edge.data().kind = kind;
            Visualization.update();
            return true;
        }

        return false;
    },

    removeNode: function(name) {
        Visualization.cy.nodes('[name="' + name + '"]').remove();
    },

    removeEdge: function(from, to) {
        from = Visualization.nodeNameToId(from);
        to = Visualization.nodeNameToId(to);
        Visualization.cy.edges('[source="' + from + '"][target="' + to + '"]').remove();
    },

    update: function() {
        Visualization.cy.style().update();
    },

    cytoscapeOptions: function(domElements) { return {
        container: domElements.cyContainer,
        layout: {
            animate: true,
            animationDuration: 300,
            animationThreshold: 250,
            refresh: 20,
            fit: true,
            name: 'cose',
            padding: 250,
            animate: true,
            nodeRepulsion: function(node) { return 100000; },
            nodeDimensionsIncludeLabels: true,
        },
        style: [{
            'selector': 'node[name]',
            'style': {
                'overlay-opacity': 0,
                'text-valign': 'center',
                'text-halign': 'center',
                'width': 'label',
                'height': 'label',
                'padding': 6,
                'label': 'data(name)',
                'shape': 'barrel',
                'background-color': '#dddddd',
                'font-family': 'FiraMono',
                'font-size': '10pt',
                'border-width': '0.5px',
                'border-color': '#bbbbbb',
                'border-style': 'solid'
            }
        },
        {
            'selector': 'node[hasUpdateFn="true"]',
            'style': {
                'background-color': '#f4f4f4'
            }
        },
        {
            'selector': 'node[hoverFromMenu="true"]',
            'style': {
                'border-style': 'dashed',
                'border-color': '#6a7ea5',
                'border-width': '0.8px'
            }
        },
        {
            'selector': 'edge',
            'style': {
                'width': 2.4,
                'curve-style': 'bezier',
                'loop-direction': '-15deg',
                'loop-sweep': '30deg',
                'text-outline-width': 2.3,
                'text-outline-color': '#cacaca',
                'font-family': 'FiraMono',
            }
        },
        {
            'selector': 'edge[visible]',
            'style': {
                'label': 'data(visible)',
            }
        },
        {
            'selector': 'edge[kind="activation"]',
            'style': {
                'line-color': domElements.constants.niceGreen,
                'target-arrow-color': domElements.constants.niceGreen,
                'target-arrow-shape': 'triangle'
            }
        },
        {
            'selector': 'edge[kind="inhibition"]',
            'style': {
                'line-color': domElements.constants.niceRed,
                'target-arrow-color': domElements.constants.niceRed,
                'target-arrow-shape': 'tee',
            }
        },
        {
            'selector': 'edge[kind="unspecified"]',
            'style': {
                'line-color': domElements.constants.niceGrey,
                'target-arrow-color': domElements.constants.niceGrey,
                'target-arrow-shape': 'triangle',
            }
        },
        {
            'selector': 'node:selected',
            'style': {
                'border-style': 'dashed',
                'border-color': '#6a7ea5',
                'border-width': '0.8px'
            }
        },
        {
            'selector': 'edge:selected',
            'style': {
                'line-style': 'dashed',
                'line-dash-pattern': [8, 1],
            }
        },
        /* edge handles style */ 
        {
            'selector': '.eh-handle',
            'style': {
                'background-color': domElements.constants.niceBlue,
                'color': '#f9f9f9',
                'width': 14,
                'height': 14,
                'shape': 'diamond',
                'font-family': 'Fira Mono',
                'padding': 0,
                'overlay-opacity': 0,
                'border-width': 0,
                'border-opacity': 0,
                'label': 'add',
                'text-valign': 'center',
                'text-halign': 'center',
                'font-size': '5pt',
            }
        },
        {
            'selector': '.eh-hover',
            'style': {}
        },
        {
            'selector': '.eh-source',
            'style': {}
        },
        {
            'selector': '.eh-target',
            'style': {}
        },
        {
            'selector': '.eh-preview, .eh-ghost-edge',
            'style': {
                'background-color': domElements.constants.niceGrey,
                'line-color': domElements.constants.niceGrey,
                'target-arrow-color': domElements.constants.niceGrey,
                'target-arrow-shape': 'triangle',
            }
        },
        {
            'selector': '.eh-ghost-edge.eh-preview-active',
            'style': {
                'opacity': 0
            }
        }],
    };},

    edgehandlesOptions: {
        preview: true, // whether to show added edges preview before releasing selection
        hoverDelay: 150, // time spent hovering over a target node before it is considered selected
        handleNodes: 'node', // selector/filter function for whether edges can be made from a given node
        snap: false,
        snapThreshold: 50,
        snapFrequency: 15,
        noEdgeEventsInDraw: false,
        disableBrowserGestures: true, 
        handlePosition: function(node) { return 'middle top'; },
        handleInDrawMode: false,
        edgeType: function(sourceNode, targetNode) { return 'flat'; },
        loopAllowed: function(node) { return true; },
        nodeLoopOffset: -50,
        edgeParams: function(sourceNode, targetNode, i) {
            return { data: { visible: '', kind: 'unspecified' }};
        },
        complete: function(sourceNode, targetNode, addedEles) {
            if (!Model.existsEdge(sourceNode.data().name, targetNode.data().name)) {
                Model.addEdge(sourceNode.data().name, targetNode.data().name, true, 'unspecified');
            } else {
                addedEles.remove();
            }
        },
    },

    cy: undefined,
    eh: undefined,
    layoutChangeEventFunctions: [],
    layoutName: 'custom',
    onRename: [],
};
