
function DEBUG_init(i = 1) {
    var models = [
       `m2c -|? p53
        dna ->  dna
        m2c ->  m2n
        p53 ->  m2c
        p53 -|  dna
        p53 -|  m2n
        dna -|  m2n`,

       `Glutamate_Receptor -> Calcium
        Glutamate_Receptor -> Packaging_Proteins
        Calcium -> Calcineurin
        Phospholipase_C -> Calcium
        Packaging_Proteins -> Exocytosis
        DARPP32 -| Protein_Phosphatase_1
        Protein_Phosphatase_1 -| Glutamate_Receptor
        Glutamate -> Glutamate_Receptor
        Glutamate -> Glutamate
        Protein_Kinase_A -> Glutamate_Receptor
        Protein_Kinase_A -> DARPP32
        Calcineurin -| DARPP32
        Dopamine -> COMT
        Dopamine -> Dopamine_Receptor_1
        Dopamine -> Dopamine_Receptor_2
        COMT -| Dopamine
        Dopamine_Receptor_1 -> Adenylate_cyclase
        Adenylate_cyclase -> Protein_Kinase_A
        Dopamine_Receptor_2 -> Phospholipase_C
        Dopamine_Receptor_2 -| Adenylate_cyclase
        Tryosine_hydroxylase -> Tryosine_hydroxylase
        Tryosine_hydroxylase -> Dopamine`
    ];
    DomElements.generateResultTable([{"sat_count":42,"phenotype":["DIS", "STABILITY",
               "DISORDER", "OSCILLATION"]},{"sat_count":28,"phenotype":["STABILITY"]},{
               "sat_count":2900,"phenotype":["DISORDER", "STABILITY", "STA", "O", "D"]},{"sat_count":2, "phenotype":["O","D","O"]}])
    Model.ImportExport.fromString(models[i]);
    Visualization.fromModel();
    Visualization.layout('cose');
    Visualization.cy.fit();
}

/* main initialization function, called on load */
function init() {
    DomElements.init();
    Visualization.initialize();
    
    //DEBUG_init(0);

    DomElements.updateEnginePanel();
    DomElements.updateModelInfo();
    DomElements.updateModelTable();
    DomElements.updateImportExportInfo();
    DomElements.modelHasChangedShow(false);

    Visualization.addOnLayoutChangeEvent(DomElements.updateLayoutInfo);
    Visualization.cy.on('click', ev => GuiClick.addNodeDoubleClick(ev));
    Visualization.cy.zoom(3);
    Model.addOnChangeEvent(DomElements.updateModelPanel);
    Model.addOnChangeEvent(DomElements.updateEnginePanel);
    Model.addOnChangeEvent(DomElements.updateImportExportInfo);
    Model.addOnChangeEvent(DomElements.modelHasChangedMessage);
    try {
        localStorage.setItem('localstorage?', 'yes');
        Model.addOnChangeEvent(() => localStorage.setItem('model', Model.ImportExport.toString()));
    } catch (e) {}

    // should load some model from the engine?
    if (window.location.hash.length != 0) {
        console.log('loading from server');
        Backend.getModel(window.location.hash.slice(1));    
        window.location.hash = '';
        GuiClick.connectToBackend();
        DomElements.showPanel('model');
    }
}

function updateMenu() {
//   console.log(':deprecated update menu');
//   var element = selected();
//   if (element.length == 0) {
//       return;
//   }
//   var pos = undefined;
//   var menu = undefined;
//   var size = element.renderedHeight() / 2;
//
//   if (element.is('node')) {
//       menu = document.getElementById('clickNodeMenu');
//       pos = element.renderedPosition();
//   } else if (element.is('edge')) {
//       menu = document.getElementById('clickEdgeMenu');
//       size = 0;
//       var x1 = cy.filter('node[id="' + element.data().source + '"]').renderedPosition();
//       var x2 = cy.filter('node[id="' + element.data().target + '"]').renderedPosition();
//       pos = { x: (x1.x + x2.x) / 2, y: (x1.y + x2.y) / 2 };
//   } 
//
//   menu.style.left = pos.x + 'px';
//   menu.style.top = pos.y + size + 'px';
}
