
let GuiClick = { 
    removeNode: function(name) {
        DomElements.selectionNodeMenu.style.display = 'none';
        if (name && confirm('Do you really want to delete species ' + name + '?')) {
            Model.removeNode(name);
            Visualization.removeNode(name);
        }
    },

    removeEdge: function(sourceTarget) {
        DomElements.selectionEdgeMenu.style.display = 'none';
        if (sourceTarget == null) {
            return;
        }
        Model.removeEdge(sourceTarget.source, sourceTarget.target);
        Visualization.removeEdge(sourceTarget.source, sourceTarget.target);
        Visualization.update();
    },

    selectElement: function(elementEvent, selected = true) {
        const isNode = elementEvent.target.is('node');
        
        DomElements.selectionNodeMenu;
        DomElements.selectionEdgeMenu;

        if (selected && isNode) {
            const position = elementEvent.target.renderedPosition();
            DomElements.selectionNodeMenu.style.display = 'block';
            DomElements.selectionNodeMenu.style.left = position.x + 'px';
            DomElements.selectionNodeMenu.style.top = position.y + 'px';
        }

        if (selected && !isNode) {
            const position = elementEvent.target.renderedBoundingBox();
            console.log(position);
            DomElements.selectionEdgeMenu.style.display = 'block';
            DomElements.selectionEdgeMenu.style.left = (position.x1 + position.x2) / 2 + 'px';
            DomElements.selectionEdgeMenu.style.top = (position.y1 + position.y2) / 2 + 'px';
        }

        if (!selected) {
            DomElements.selectionNodeMenu.style.display = 'none';
            DomElements.selectionEdgeMenu.style.display = 'none';
        }

        if (selected) {
            console.log(':selected ', elementEvent, 'node?', (isNode? 'yes':'no'));
        } else {
            console.log(':unselected ', elementEvent.target.position());
        }
    },

    renameNode: function() {
        console.log(':renaming node');
    },

    hide(elementId) {
        document.getElementById(elementId).style.display = 'none';
    },  

    cancelRenameMenu: function() {
        DomElements.showRenameMenu();
    },  

    getWitness: function(code) {
        const url = window.location.origin + window.location.pathname;
        window.open(url + '#' + code);
    },

    renameSpecies(oldName, newName) {
        newName = newName.trim().replace(/\s/g, '_');
        Model.renameNode(oldName, newName)
        Visualization.renameNode(oldName, newName)
        DomElements.updateModelPanel();
    },

    changeSpeciesUpdateFn: function(species, updateFn) {
        Model.addNodeConstraint(species, updateFn);
        Visualization.addNodeConstraint(species, updateFn.trim().length == 0? false: true);
    },

    toggleRegulationObservability: function(from, to, element) {
        var current = element.innerText;
        Visualization.changeEdgeObservability(from, to, current == 'observable'? '?': '');
        Model.changeEdgeObservability(from, to, current != 'observable');
        element.innerText = current == 'observable'? 'non-observable': 'observable';
    },

    toggleRegulationKind: function(from, to, element) {
        var current = element.innerText;
        var kinds = { activation: { name: 'inhibition', label: 'inhibition' },
                      inhibition: { name: 'unspecified', label: 'regulation' },
                      regulation: { name: 'activation', label: 'activation'} };
        Visualization.changeEdgeKind(from, to, kinds[current].name);
        Model.changeEdgeKind(from, to, kinds[current].name);
        element.innerText = kinds[current].label;
    },

    importFromAeon: function(inputElement) {
        var file = inputElement.files[0];
        var fr = new FileReader();
        fr.onload = (e) => {
            Model.ImportExport.fromString(e.target.result);
            Visualization.fromModel();
            Visualization.layout('cose');
        };
        fr.readAsText(file);
    },

    submitRenameMenu: function() {
        var oldName = selected().data().id;
        var newName = DomElements.renameTextInputBox.value;

        cy.filter('edge[target="' + oldName + '"]').map(x => x.data().target = newName);
        cy.filter('edge[source="' + oldName + '"]').map(x => x.data().source = newName);

        DomElements.showRenameMenu();
        selected().data().id = newName;
    },  

    speciesCellHover: function(name, show = true) {
        var graphElement = Visualization.cy.nodes('[name="' + name + '"]');
        if (graphElement.length == 0) {
            return;
        }
        var htmlElement = document.getElementById('speciesCell' + name);
        if (show) {
            graphElement.data().hoverFromMenu = 'true'; 
            htmlElement.style.outline = '#6a7ea5 dashed 2px';
        } else {
            graphElement.data().hoverFromMenu = 'false'; 
            htmlElement.style.outline = 'none';
        }
        Visualization.cy.style().update()
    },

    engineMenu: function() {
        console.log(':engine menu');
    },  

    importExportMenu: function() {
        console.log(':import/export menu');
    },  

    layoutMenu: function() {
        console.log(':layout menu');
        var layouts = ['cose', 'grid', 'circle', 'random'];
        var newLayout = layouts[(this.currentLayout + 1) % layouts.length]
        this.currentLayout += 1;
        cy.layout({ animate: true, animationDuration: 300, animationThreshold: 250,
                refresh: 20, fit: true, name: newLayout }); 

    },  

    editAsTextMenu: function(action = 'show') {
        console.log(':edit as text');
        if (action == 'show') {
            DomElements.textInputWindow.style.display = 'block';
            DomElements.textInputBox.value = Model.ImportExport.toString();
        }

        if (action == 'submit') {
            DomElements.textInputWindow.style.display = 'none';
            Model.ImportExport.fromString(DomElements.textInputBox.value);
        }

        if (action == 'cancel') {
            DomElements.textInputWindow.style.display = 'none';
        }
    },  

    toggleRegulationObservabilityFromEditor: function() {
        var edge = Visualization.sourceTargetOfSelectedEdge();
        if (edge == null) {
            return;
        }

        var current = Model.getRegulation(edge.source, edge.target).visible;
        Visualization.cy.edges('[source="' + Visualization.nodeNameToId(edge.source) + '"][target="' + Visualization.nodeNameToId(edge.target) + '"]').data().visible = current? '?': '';
        Visualization.update();
        Model.changeEdgeObservability(edge.source, edge.target, !current);
        DomElements.updateModelPanel();
    },  

    toggleRegulationKindFromEditor: function() {
        var edge = Visualization.sourceTargetOfSelectedEdge();
        if (edge == null) {
            return;
        }
        var kinds = { activation: 'inhibition',
                      inhibition: 'unspecified',
                      unspecified: 'activation' };
        var current = Model.getRegulation(edge.source, edge.target).kind;
        Visualization.changeEdgeKind(edge.source, edge.target, kinds[current]);
        Model.changeEdgeKind(edge.source, edge.target, kinds[current]);
        DomElements.updateModelPanel();
    },

    addFreshNode: function(position) {
        Visualization.addNode('newNode' + GuiClick.lastNewNodeId, position);
        Model.addNode('newNode' + GuiClick.lastNewNodeId);

        GuiClick.lastNewNodeId++;
    },  

    layout: function(name) {
        Visualization.layout(name);
    },

    exportAeon: function() {
        Model.ImportExport.toFile();
    },

    importLocalStorage: function() {
        var model = null;
        if (model = localStorage.getItem('model')) {
            Model.ImportExport.fromString(model);
            Visualization.fromModel();
            Visualization.layout('cose');
        }
    },

    connectToBackend: function() {
        Backend.getInfo();
        DomElements.updateEnginePanel();
    },

    addNodeDoubleClick: function(ev) {
        var timeTresh = 400;
        var now = (new Date()).getTime();

        if (now - GuiClick.lastClickTimestamp <= timeTresh) {
            GuiClick.addFreshNode(ev.position);
        }

        GuiClick.lastClickTimestamp = now;
    },

    modifyNodeParams: function() {
        console.log(':modify node params.');
        var name = selected().data().id;
        var edges = cy.elements('edge[target= "' + name + '"]');
        var activatorNames = edges.map(x => x.data().source);
    },

    updateSelectionMenuPosition: function() {
      //  console.log(':update selection menu position');
      //  var element = selected();
      //  if (element.length == 0) {
      //      return;
      //  }
      //  var pos = undefined;
      //  var menu = undefined;
      //  var size = element.renderedHeight() / 2;

      //  if (element.is('node')) {
      //      menu = document.getElementById('clickNodeMenu');
      //      pos = element.renderedPosition();
      //  } else if (element.is('edge')) {
      //      menu = document.getElementById('clickEdgeMenu');
      //      size = 0;
      //      var x1 = cy.filter('node[id="' + element.data().source + '"]').renderedPosition();
      //      var x2 = cy.filter('node[id="' + element.data().target + '"]').renderedPosition();
      //      pos = { x: (x1.x + x2.x) / 2, y: (x1.y + x2.y) / 2 };
      //  }

      //  menu.style.left = pos.x + 'px';
      //  menu.style.top = pos.y + size + 'px';
    },

    lastClickTimestamp: 0,
    lastNewNodeId: 0,
    currentLayout: 0,
};

